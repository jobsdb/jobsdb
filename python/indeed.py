from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random
from db import connection, getFirstResult, sqlInsert, sqlInsertResult
import re

options = FirefoxOptions()
profile = FirefoxProfile(r'C:\Users\jobs\AppData\Roaming\Mozilla\Firefox\Profiles\9hsfl8ap.Indeed.com')
# options.headless = True

options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'

driver = Firefox(
    firefox_profile=profile,
    executable_path=r"C:\Users\jobs\Documents\gecko\geckodriver.exe",
    options=options,
)

driver.maximize_window()

locations = ['Chicago%2C+IL', 'Washington%2C+DC', 'New+York%2C+NY', 'Miami%2C+FL']
keywords = 'Chinese; Consultant; Project Manager; Programme Coordinator; Business Analyst; Policy Analyst; International Affairs; Social Sciences;'
keywords = str(keywords).split(";")

for l in locations:

    for kw in keywords:

        start = 0
        while True:
            url = "https://www.indeed.com/jobs?q=" + str(kw) + "&l=" + str(l) + "&start=" + str(start)
            driver.get(url)
            time.sleep(5)

            if('An error has occurred.</span>This page has lost the will to work.' in driver.page_source):
                break
            
            jobs = []
            try:
                articles = driver.find_elements(By.CLASS_NAME, "jcs-JobTitle")
                for i in articles:
                    html = i.get_attribute("outerHTML")

                    soup = BeautifulSoup(html, 'html.parser')
                    jobId = soup.find("a")['id']
                    jobId = str(jobId).replace("job_", "")
                    href = soup.find("a")['href']

                    jobs.append(jobId)                        

            except:
                continue

            for jobId in jobs:

                driver.get("https://www.indeed.com/viewjob?jk=" + jobId)
                
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                desc = soup.find("div", {"id": "jobDescriptionText"})
                title = soup.find("h1").text
                desc = desc.text
                emails = re.findall(r'[\w.+-]+@[\w-]+\.[\w.-]+', desc)
                if(len(emails) > 1):
                    email = emails[0]

                    sql = "INSERT INTO `emails` (`email`, `title`, `applicantId`) VALUES (%s, %s, %s);"
                    val = (email, title, 1)
                    sqlInsertResult(sql, val)

                sql = "INSERT INTO `indeed_jobs` (`jobId`, `applicant`) VALUES (%s, %s);"
                val = (jobId, 1)
                sqlInsertResult(sql, val)



            start = start + 10


