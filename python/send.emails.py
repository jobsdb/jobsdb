from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random
import pyautogui
from subs import send_email
from db import connection, getFirstResult, sqlInsert, sqlInsertResult

sql = "SELECT id, email, title FROM `emails` WHERE `sent` = 0"
conn = connection()
mycursor1 = conn.cursor()
mycursor1.execute(sql)
myresult1 = mycursor1.fetchall()
mycursor1.close()
conn.close()

for x1 in myresult1:
    id = x1[0]
    email = x1[1]
    title = x1[2]

    subject = "Applying to " + title
    mail_content = 'Hi,\n\nI am a software developer looking for a new position if you are interested\n\n'

    send_email(title, mail_content, email)

    sql = "UPDATE `emails` SET `sent` = '0' WHERE `emails`.`id` = 2;"
    sqlInsert(sql)