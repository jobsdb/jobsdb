from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random
from db import connection, getFirstResult, sqlInsert, sqlInsertResult
import re

options = FirefoxOptions()
profile = FirefoxProfile(r'C:\Users\jobs\AppData\Roaming\Mozilla\Firefox\Profiles\c6frbrli.finn.no')
# options.headless = True

options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'

driver = Firefox(
    firefox_profile=profile,
    executable_path=r"C:\Users\jobs\Documents\gecko\geckodriver.exe",
    options=options,
)

driver.maximize_window()

page = 1
while True:
    url = "https://www.finn.no/job/fulltime/search.html?industry=65&page=" + str(page)
    driver.get(url)
    
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    results = soup.find("section", {"id": "page-results"})
    try:
        articles = results.find_all("article")
    except:
        # Last page
        break
    for a in articles:
        url = a.find("a")['href']
        try:
            code = url.split("finnkode=",1)[1]
        except:
            continue

        found = getFirstResult("SELECT * FROM `jobs` WHERE `url` = '" + str(url) + "'")
        if(found != "NOT_FOUND"):
            continue

        driver.get(url)

        soup = BeautifulSoup(driver.page_source, 'html.parser')

        job_title = soup.find("h1").text
        try:
            company_name = soup.find("dl", {"class": "definition-list"}).find_all("dd")[0].text
        except:
            continue

        # Check for emails
        emails = re.findall(r'[\w.+-]+@[\w-]+\.[\w.-]+', driver.page_source)
        if(len(emails) >= 2):
            email = emails[1]

            sql = "INSERT INTO `emails` (`email`, `title`) VALUES (%s, %s);"
            val = (email, job_title)
            sqlInsertResult(sql, val)

        applied = 0
        if('Denne annonsen har FINNs "Enkel søknad"' in driver.page_source):

            driver.get("https://www.finn.no/job/apply?adId=" + str(code))
            time.sleep(3)

            name = driver.find_element(By.ID, "name")
            name.clear()
            name.send_keys("Simen Nilsen")
            email = driver.find_element(By.NAME, "email")
            email.clear()
            email.send_keys("ssgnilsen3@gmail.com")
            tel = driver.find_element(By.NAME, "phone")
            tel.clear()
            tel.send_keys("46514482")
            occupation = driver.find_element(By.NAME, "occupation")
            occupation.clear()
            occupation.send_keys("Fullstack developer")

            select = Select(driver.find_element(By.ID,  "education"))
            select.select_by_index(2)

            input = driver.find_element(By.ID, "attachment-container-cv")
            try:
                input.find_elements(By.CLASS_NAME, "rounded-16")[1].click()
            except:
                input.find_elements(By.CLASS_NAME, "rounded-16")[0].click()
            
            cv_input = driver.find_element(By.ID, "attachmentUpload-cv")
            

            cv_input.send_keys(r"H:\My Drive\CV-robot\CV-Simen-Nilsen-Frontend-Backend-Developer_CV_Compressed.pdf")
            time.sleep(5)

            # Answer all questions
            try:
                questions = driver.find_element(By.ID, "question-container")
                fieldsets = questions.find_elements(By.TAG_NAME, "fieldset")
                for f in fieldsets:
                    input = f.find_element(By.TAG_NAME, "label")
                    input.click()
            except:
                pass

            driver.find_element(By.ID, "applicationLetter").send_keys("Hei, jeg er en IT utvikler som ser etter en ny jobb.")
            driver.find_element(By.CLASS_NAME, "button--primary").click()
            time.sleep(3)
            driver.find_element(By.ID, "submitApplication").click()
            time.sleep(5)
            applied = 1
        
        sql = "INSERT INTO `jobs` (`title`, `company`, `applied`, `source`, `url`) VALUES (%s, %s, %s, %s, %s);"
        val = (job_title, company_name, applied, "Finn.no", url)
        sqlInsertResult(sql, val)       


    if('Ingen treff akkurat nå</h2>' in driver.page_source):
        driver.quit()
        break

    page = page + 1


