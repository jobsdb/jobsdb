from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random
from db import connection, getFirstResult, sqlInsert, sqlInsertResult
import re
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from email.mime.application import MIMEApplication
from email.utils import formataddr

options = FirefoxOptions()
profile = FirefoxProfile(r'C:\Users\jobs\AppData\Roaming\Mozilla\Firefox\Profiles\y2kmrpic.jobs.ch')
# options.headless = True

options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'

driver = Firefox(
    firefox_profile=profile,
    executable_path=r"C:\Users\jobs\Documents\gecko\geckodriver.exe",
    options=options,
)

driver.maximize_window()

page = 1
while True:
    url = "https://www.jobs.ch/en/vacancies/information-technology-telecom/?page=" + str(page)
    driver.get(url)
    print(url)
    time.sleep(5)

    if(page == 101):
        break
    
    # try:
    articles = driver.find_elements(By.CLASS_NAME, "VacancyLink___StyledLink-sc-ufp08j-0")
    jobIds = []
    for i in articles:
        html = i.get_attribute("outerHTML")
        soup = BeautifulSoup(html, 'html.parser')
        href = soup.find("a")['href']
        jobId = href.split("/")
        jobId = jobId[4]

        # Check if jobId is found
        result = getFirstResult("SELECT * FROM `jobs` WHERE `jobId` = '" + str(jobId) + "'")
        if(result != "NOT_FOUND"):
            continue

        jobIds.append(jobId)
    
    for jobId in jobIds:

        url = "https://www.jobs.ch/en/vacancies/detail/" + str(jobId) + "/?source=vacancy_search_promo"
        driver.get(url)
        time.sleep(3)

        soup = BeautifulSoup(driver.page_source, 'html.parser')

        try:
            company = soup.find_all("h3", {"data-cy": "company-name"})
            company = company.pop()
            company_name = company.text
        except:
            company_name = None
        job_title = soup.find("h1").text

        # Get email

        try:
            iframe = driver.find_element(By.ID, "vacancy-frame-" + str(jobId))
            if(iframe != None):
                driver.switch_to.frame(iframe)
                time.sleep(1)
        except:
            pass
            
        emails = re.findall(r'[\w.+-]+@[\w-]+\.[\w.-]+', driver.page_source)
        if(len(emails) > 1):
            email = re.findall(r'[\w.+-]+@[\w-]+\.[\w.-]+', driver.page_source)[0]
            sql = "INSERT INTO `emails` (`email`, `title`) VALUES (%s, %s);"
            val = (email, job_title)
            sqlInsertResult(sql, val)

        driver.switch_to.default_content()

        applied = 0
        try:
            email_btn = soup.find("div", {"class": "VacancyContactLinks___StyledFlex-sc-1shy3n2-1"}).find("button")['data-cy']
            if(email_btn != None):
                # Send CV by msg

                driver.get("https://www.jobs.ch" + email_btn)
                time.sleep(3)
                driver.find_element(By.NAME, "email").send_keys("ssgnilsen3@gmail.com")
                driver.find_element(By.NAME, "firstname").send_keys("Simen")
                driver.find_element(By.NAME, "lastname").send_keys("Nilsen")
                driver.find_element(By.ID, "motivationalLetter").send_keys("Hi, I am a programmer looking for a new job.")

                input = driver.find_element(By.CLASS_NAME, "DocumentUpload___StyledFieldset-sc-cv8sw-1")
                input = input.find_element(By.TAG_NAME, "input")
                input.send_keys(r"H:\My Drive\CV-robot\CV-Simen-Nilsen-Frontend-Backend-Developer_CV_Compressed.pdf")
                time.sleep(5)
                
                btns = driver.find_elements(By.TAG_NAME, "button")
                for b in btns:
                    html = b.get_attribute("outerHTML")
                    if("send-application-button" in html):
                        b.click()
                        break
                
                time.sleep(3)
                applied = 1
        except:
            pass


        sql = "INSERT INTO `jobs` (`title`, `company`, `applied`, `source`, `url`, `jobId`) VALUES (%s, %s, %s, %s, %s, %s);"
        val = (job_title, company_name, applied, "Jobs.ch", url, jobId)
        sqlInsertResult(sql, val)

            

   
    page = page + 1


