from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random

options = FirefoxOptions()
profile = FirefoxProfile(r'C:\Users\jobs\AppData\Roaming\Mozilla\Firefox\Profiles\zazz9jsi.default-release')
# options.headless = True

options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'

driver = Firefox(
    firefox_profile=profile,
    executable_path=r"C:\Users\jobs\Documents\gecko\geckodriver.exe",
    options=options,
)

driver.maximize_window()

# Submit jobs

jobs_db_sites = ['hk.jobsdb.com', 'www.seek.com.au', 'www.jobstreet.co.id', 'www.jobstreet.com.my', 'www.seek.co.nz', 'jobstreet.com.ph', 'www.jobstreet.com.sg']
random.shuffle(jobs_db_sites)

for site in jobs_db_sites:
        
    driver.get("https://" + site)
    time.sleep(5)

    # Sign in

    try:
        driver.find_element(By.XPATH, "//a[@data-automation='sign in']").click()
        time.sleep(5)
        driver.find_element(By.NAME, "emailAddress_seekanz").send_keys("ssg.nilsen@gmail.com")
        driver.find_element(By.NAME, "password_seekanz").send_keys("opijo123")
        driver.find_element(By.XPATH, "//button[@data-cy='login']").click()
        time.sleep(10)
    except:
        pass

    # Upload new CV
    

    page = 1
    companies = []
    while True:

        driver.get("https://" + str(site) + "/jobs-in-information-communication-technology?sortmode=ListedDate&page=" + str(page))    
        time.sleep(5)

        if("No matching search results" in driver.page_source):
            break

        soup = BeautifulSoup(driver.page_source, 'html.parser')
        jobs = soup.find_all("article", {"data-card-type": "JobCard"})

        for j in jobs:

            if('data-automation="applied-job"' in str(j)):
                continue

            links = j.find_all("a", {"data-automation": "jobTitle"})
            for l in links:
                href = l['href']

                if("/job/" in href):
                    break
            
            try:
                company = j.find("a", {"data-automation": "jobCompany"}).text
                if(company in companies):
                    continue
            except:
                continue

            companies.append(company)

            driver.get("https://" + str(site) + href)
            time.sleep(5)
            link = driver.find_element(By.XPATH, "//a[@data-automation='job-detail-apply']")
            html = link.get_attribute("outerHTML")
            if("_blank" in html):
                print("External job")
                continue
            else:
                link.click()

            time.sleep(10)
            
            try:
                driver.find_element(By.XPATH, "//input[@data-testid='coverLetter-none']").click()
                time.sleep(1)
            except:
                print("External job")
                continue

            driver.find_element(By.XPATH, "//button[@data-testid='continue-button']").click()
            time.sleep(3)

            try:
                driver.find_element(By.ID, "newToWorkforce-:r7:").click()
            except:
                pass

            driver.find_element(By.XPATH, "//button[@data-testid='continue-button']").click()
            time.sleep(1)

            
            try:
                driver.find_element(By.ID, "newToWorkforce-:r7:").click()
            except:
                pass

            try:
                driver.find_element(By.XPATH, "//button[@data-testid='continue-button']").click()
                time.sleep(1)
            except:
                pass

            try:
                driver.find_element(By.XPATH, "//button[@data-testid='review-submit-application']").click()
                time.sleep(5)
                print("Applied to job")
            except:
                print("Failed to apply to job")
            
            

        page = page + 1   