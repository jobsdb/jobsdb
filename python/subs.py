import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from email.mime.application import MIMEApplication
from email.utils import formataddr
import time
from selenium.webdriver.common.by import By

def send_email(title, mail_content, receiver_address):

    # Generete app passwords
    # https://support.google.com/mail/answer/185833?hl=en

    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = formataddr(('Simen Nilsen', 'simennilsen73@gmail.com'))
    message['To'] = receiver_address
    message['Subject'] = str(title)

    
    #The subject line
    #The body and the attachments for the mail
    message.attach(MIMEText(mail_content, 'plain'))
    file = "G:\My Drive\CV\CV-Simen-Nilsen-Frontend-Backend-Developer_compressed.pdf"
    with open(file, 'rb') as f:
        part = MIMEApplication(
                f.read(),
                Name=f.name
            )
            

    part['Content-Disposition'] = 'attachment; filename="%s"' % "CV-Simen-Nilsen-Frontend-Backend-Developer.pdf"
    message.attach(part)

    # try:
        #Create SMTP session for sending the mail
    session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
    session.starttls() #enable security
    session.login('simennilsen73@gmail.com', 'brykncsudqzvexzb') #login with mail_id and password
    text = message.as_string()
    r = session.sendmail('simennilsen73@gmail.com', receiver_address, text)
    session.quit()
    # print('Mail Sent ' + str(email))
    # except:
    #     pass

def send_emails_outlook(driver):
    
    driver.get("https://outlook.live.com/mail/0/")

    # Locate Ribbon

    while True:
        try:
            ribbon = driver.find_element(By.ID, "innerRibbonContainer")
            time.sleep(10)
            break
        except:
            pass

    btns = ribbon.find_elements(By.TAG_NAME, "button")
    btns[0].click()
    time.sleep(2)
    msgbox = driver.find_element(By.ID, "ReadingPaneContainerId")
    to = msgbox.find_element(By.XPATH, "//div[@aria-label='To']")
    to.send_keys("ssgnilsen3@gmail.com")
    time.sleep(5)
    driver.find_element(By.ID, "FloatingSuggestionsFooterId2").click()
    time.sleep(1)
    to = msgbox.find_element(By.XPATH, "//input[@aria-label='Add a subject']")
    to.send_keys("Good Day")
    editor = msgbox.find_element(By.ID, "editorParent_1") 
    editor.click()
    pyautogui.write("123")
    time.sleep(3)

    # Add attachment
    btns = driver.find_elements(By.CLASS_NAME, "ms-OverflowSet-item")
    btns[4].click()
    time.sleep(1)
    driver.find_element(By.XPATH, "//button[@aria-label='Attach file']").click()
    time.sleep(1)
    onedrives = driver.find_elements(By.XPATH, "//button[@aria-label='OneDrive']")
    onedrives[1].click()
    time.sleep(2)
    modal = driver.find_element(By.CLASS_NAME, "ms-Dialog-main")
    scrollbar = modal.find_element(By.CLASS_NAME, "customScrollBar")
    scrollbar.find_element(By.XPATH, "//div[@data-selection-index='0']").click()
    modal.find_element(By.CLASS_NAME, "ms-Button--primary").click()
    time.sleep(5)
    driver.find_element(By.XPATH, "//button[@aria-label='Send']").click()
    time.sleep(15123)

    time.sleep(500)