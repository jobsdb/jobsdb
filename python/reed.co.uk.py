from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxProfile
import random
from db import connection, getFirstResult, sqlInsert, sqlInsertResult

options = FirefoxOptions()
profile = FirefoxProfile(r'C:\Users\jobs\AppData\Roaming\Mozilla\Firefox\Profiles\xf3et8i2.reed.co.uk')
# options.headless = True

options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'

driver = Firefox(
    firefox_profile=profile,
    executable_path=r"C:\Users\jobs\Documents\gecko\geckodriver.exe",
    options=options,
)

driver.maximize_window()

# Upload new CV

url = "https://www.reed.co.uk/candidate-profile"
driver.get(url)
time.sleep(5)
driver.find_element(By.CLASS_NAME, "update-cv_card__header__TT7vn").find_element(By.TAG_NAME, "button").click()
time.sleep(1)
driver.find_element(By.CLASS_NAME, "update-modal_modalBody__Rz71S").find_element(By.TAG_NAME, "button").click()
time.sleep(1)
driver.find_element(By.CLASS_NAME, "add-cv_uploadContainer__7qUKX").find_element(By.TAG_NAME, "input").send_keys(r"H:\My Drive\CV-robot\CV-Simen-Nilsen-Frontend-Backend-Developer_CV_Compressed.pdf")
while True:
    try:
        driver.find_element(By.CLASS_NAME, "change-cv_modalHeader__Hxkt8").find_element(By.TAG_NAME, "button").click()
        break
    except:
        pass

page = 1
while True:
    
    url = "https://www.reed.co.uk/jobs/it-jobs?sortBy=displayDate&pageno=" + str(page) + "&isEasyApply=true"
    driver.get(url)
    time.sleep(5)

    if('An error has occurred.</span>This page has lost the will to work.' in driver.page_source):
        break
    
    try:
        articles = driver.find_elements(By.CLASS_NAME, "job-card_jobCard__body__86jgk")
        for i in articles:
            html = i.get_attribute("outerHTML")
            if('Applied</button>' in html):
                continue
            btn = i.find_element(By.CLASS_NAME, "job-card_applyBtn__2N2jy")
            btn.click()
            time.sleep(3)
            driver.find_element(By.CLASS_NAME, "apply-job-modal_buttonGroup__button__6ObMn").click()
            time.sleep(5)
            driver.find_element(By.CLASS_NAME, "application-confirmation-modal_continueButton__i73Dl").click()
            time.sleep(10)
    except:
        continue

    page = page + 1


